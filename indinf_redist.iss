;##############################################################################################################
; Setup Install script for Indirect Influences
; Jorge CATUMBA
;##############################################################################################################
; modify this path where is toolbox_skeleton directory
#define BinariesSourcePath "C:\Programs files\scilab-5.0\contrib\indirect_influences"
;
#define Toolbox_skeleton_version "1.0"
#define CurrentYear "2011"
#define Toolbox_skeletonDirFilename "indirect_influences"
;##############################################################################################################
[Setup]
SourceDir={#BinariesSourcePath}
AppName=Indirect Influences
AppVerName=Indirect Influences version 1.55-3
DefaultDirName={pf}\{#Toolbox_skeletonDirFilename}
InfoAfterfile=readme.txt
LicenseFile=license.txt
WindowVisible=true
AppPublisher=Universidad Nacional de Colombia
BackColorDirection=lefttoright
AppCopyright=Copyright � {#CurrentYear}
Compression=lzma/max
InternalCompressLevel=normal
SolidCompression=true
VersionInfoVersion={#Toolbox_skeleton_version}
VersionInfoCompany=Universidad Nacional de Colombia
;##############################################################################################################
[Files]
Source: loader.sce; DestDir: {app}
Source: DESCRIPTION; DestDir: {app}
Source: DESCRIPTION-FUNCTIONS; DestDir: {app}
Source: changelog.txt; DestDir: {app}
Source: license.txt; DestDir: {app}
Source: readme.txt; DestDir: {app}
Source: etc\indirect_influences.quit; DestDir: {app}\etc
Source: etc\indirect_influences.start; DestDir: {app}\etc
Source: macros\buildmacros.sce; DestDir: {app}\macros
Source: macros\lib; DestDir: {app}\macros
Source: macros\names; DestDir: {app}\macros
Source: macros\*.sci; DestDir: {app}\macros
Source: bin\*.bin; DestDir: {app}\bin
Source: help\*.xml; DestDir: {app}\help
;Source: sci_gateway\loader_gateway.sce; DestDir: {app}\sci_gateway
;Source: sci_gateway\c\loader.sce; DestDir: {app}\sci_gateway\c
;Source: sci_gateway\c\skeleton_c.dll; DestDir: {app}\sci_gateway\c
;Source: sci_gateway\cpp\loader.sce; DestDir: {app}\sci_gateway\cpp
;Source: sci_gateway\cpp\skeleton_cpp.dll; DestDir: {app}\sci_gateway\cpp
;Source: sci_gateway\fortran\loader.sce; DestDir: {app}\sci_gateway\fortran
;Source: sci_gateway\fortran\skeleton_fortran.dll; DestDir: {app}\sci_gateway\fortran
;Source: src\c\libcsum.dll; DestDir: {app}\src\c
;Source: src\c\loader.sce; DestDir: {app}\src\c
;Source: src\fortran\libfsum.dll; DestDir: {app}\src\fortran
;Source: src\fortran\loader.sce; DestDir: {app}\src\fortran
;Source: tests\*.*; DestDir: {app}\tests; Flags: recursesubdirs
;Source: includes\*.h; DestDir: {app}\includes; Flags: recursesubdirs
;Source: locales\*.*; DestDir: {app}\locales; Flags: recursesubdirs
;Source: demos\*.*; DestDir: {app}\locales; Flags: recursesubdirs
;
;##############################################################################################################
