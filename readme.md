# Indirect Influences (Scilab toolbox)

This toolbox provides certain functions to calculate indirect influences of a given adjacency matrix of a graph. We are developing this toolbox so you can send us the bugs and issues that you find to [here](http://forge.scilab.org/index.php/p/indinf/issues/) and we will solve it as soon as posible. Moreover, some new functions will be added trought the versions of this toolbox.

## Author

This toolbox was written by Jorge Catumba Ruiz from the Universidad Nacional de Colombia and is supported by himself.

## License

This toolbox is under the GPL license, please read the license.txt file.
