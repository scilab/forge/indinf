// ====================================================================
// Copyright (C) 2012 - Jorge Catumba Ruiz
// 
// This file must be used under the terms of the GPLv3.
// This source file is licensed as described in the file license.txt, which
// you should have received as part of this distribution.
// ====================================================================

function demo_part_adj_mat()

    mprintf("How to use part_adj_mat.\n")

    // Define a real square matrix, it must be an adjacency
    // matrix of some graph you are studing.
    A = [
    0 3 5 1 0
    3 0 0 0 4
    0 7 0 7 6
    3 0 8 0 1
    0 1 3 4 0
    ];
    mprintf("\nWe create a 5x5 real matrix.\n");
    disp(A);
    
    // Define a list containing the blocks you want to build.
    mprintf("\nWe define the nodes per block on a list.\n");
    blocklist=list([1,4],[3,5]);
    disp(blocklist);

    // Compute the adjacency matrix of the partioned graph
    mprintf("\nFinally we calculate the adjacency matrix for the partitioned graph.\n")
    Apart=part_adj_mat(A,blocklist);
    disp(Apart);
    
    //
    // Load this script into the editor
    //
    filename = "demo_part_adj_mat.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );

endfunction 
demo_part_adj_mat();
clear demo_part_adj_mat;
